<?php

namespace App\Http\Controllers;

use App\Models\Phrase;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PhrasesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $phrases = Phrase::orderBy('id', 'desc')->paginate(9);
        return view('phrases.index', compact('phrases'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $phrase = new Phrase();
        $data = [
            'en' => ['phrase' =>  $request->input('phrase'),],
                'ru' => ['phrase' => $request->input('phrase'),],
                'fr' => ['phrase' => $request->input('phrase'),],
                'ar' => ['phrase' => $request->input('phrase'),],
                'it' => ['phrase' => $request->input('phrase')],
            'user_id' => $request->user()->id
        ];

        $phrase->phrase = $request->input('phrase');
        $phrase->user_id = $request->user()->id;
        $phrase->save();

        return redirect(route('phrases.index'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Phrase $phrase): \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        return view('phrases.show', compact('phrase'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Phrase $phrase): \Illuminate\Routing\Redirector|\Illuminate\Contracts\Foundation\Application|RedirectResponse
    {
        $data = $request->validate([
            'en.phrase' => ['nullable'],
            'ru.phrase' => ['nullable'],
            'fr.phrase' => ['nullable'],
            'ar.phrase' => ['nullable'],
            'it.phrase' => ['nullable']
        ]);
        $phrase->update($data);
        return back();
    }
}
