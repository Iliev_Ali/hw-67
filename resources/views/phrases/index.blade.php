@extends('layouts.app')
@section('content')

    <div class="col-12">
        <h1>{{__('Create new Phrase')}}</h1>
    </div>
    @if(auth()->check())
        <div class="col-12">
            <form method="post" action="{{route('phrases.store')}}">
                @csrf
                <div class="form-group">
                    <label for="phrase-title">{{__('Phrase')}}</label>
                    <input name="phrase" type="text" class="form-control"
                           id="phrase-title">
                </div>
                <button type="submit" class="btn btn-primary mt-3">{{__('Create phrase')}}</button>
            </form>
        </div>
    @endif

    <div class="row">
        @csrf
        @foreach($phrases as $phrase)
            <div class="col" style="padding: 35px 0 0 0;" id="delete-phrase-{{$phrase->id}}">
                <div class="card" style="width: 18rem;">
                    <div class="card-header">
                        {{ $phrase->phrase }}
                    </div>
                    <div class="card-body">
                        <a href="{{route('phrases.show', ['phrase' => $phrase])}}"
                           class="card-link">@lang('Show more') ...</a> <br>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {{--    <div class="justify-content-md-center p-5">--}}
    {{--        <div class="col-md-auto">--}}
    {{--            {{ $phrase->links() }}--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection
