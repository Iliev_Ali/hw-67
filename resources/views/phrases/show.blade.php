@extends('layouts.app')
@section('content')
    <div class="container text-center">
        <div class="row justify-content-center">
            <h1 class="text-danger">{{__('Translation of phrase')}}</h1>
            <h2 class="card-title">
                @if($phrase->translate('en'))
                    {{$phrase->translate('en')->phrase}}
                @else
                    <h5>Пока перевода нет</h5>
                @endif
            </h2>

        </div>
        <form action="{{route('phrases.update', ['phrase' => $phrase])}}" method="POST">
            @method('PUT')
            @csrf
            <div class="languages" style="display: block">
                <label for="ru" class="d-block my-3">
                    @lang('English')
                    <input type="text" name="en[phrase]"
                           @if($phrase->translate('en')) value="{{$phrase->translate('en')->phrase}}"  @endif
                           @if(!auth()->check()) disabled @endif>
                </label>
                <label for="ru" class="d-block my-3">
                    @lang('Russian')
                    <input type="text" name="ru[phrase]"
                           @if($phrase->translate('ru'))  value="{{$phrase->translate('ru')->phrase}}"  @endif
                           @if(!auth()->check()) disabled @endif>
                </label>
                <label for="ru" class="d-block my-3">
                    @lang('French')
                    <input type="text" name="fr[phrase]"
                           @if($phrase->translate('fr')) value="{{$phrase->translate('fr')->phrase}}"  @endif
                           @if(!auth()->check()) disabled @endif>
                </label>
                <label for="ru" class="d-block my-3">
                    @lang('Arabic')
                    <input type="text" name="ar[phrase]"
                           @if($phrase->translate('ar')) value="{{$phrase->translate('ar')->phrase}}"  @endif
                           @if(!auth()->check()) disabled @endif>
                </label>
                <label for="ru" class="d-block my-3">
                    @lang('Italian')
                    <input type="text" name="it[phrase]"
                           @if($phrase->translate('it')) value="{{$phrase->translate('it')->phrase}}"  @endif
                           @if(!auth()->check()) disabled @endif>
                </label>
            </div>
            @if(auth()->check())
                <button class="btn btn-outline-success mt-3">{{__('Submit')}}</button>
            @endif
        </form>
    </div>

@endsection
