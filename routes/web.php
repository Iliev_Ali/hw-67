<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\LanguageSwitcherController;
use App\Http\Controllers\PhrasesController;
use App\Http\Middleware\LanguageSwitcherMiddleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::middleware('switcher')->group(function () {
    Route::get('/', [PhrasesController::class, 'index'])->name('dashboard');

    Route::resource('phrases', PhrasesController::class);

    Auth::routes();
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});


Route::get('language/{locale}', [LanguageSwitcherController::class, 'switcher'])
    ->name('language.switcher')
    ->where('locale', 'en|ru');



